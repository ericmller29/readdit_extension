$(function(){
	var comments = $('.comment');
	var post_id = $('.usertext-edit').prev('input[name="thing_id"]').val().split('t3_')[1];
	var username = $('#header').find('.user').find('a').text();
	var readPosts = [];
	var displayedReadPosts = [];
	var dropDownMenu = $('.commentarea').find('.panestack-title');

	dropDownMenu.append('<a href="javascript: void(0)" class="title-button readdit-show-read">show already read comments</a>');

	$.get('https://readdit.eastoh.co/api/read', { username: username, post_id: post_id }, function(data){
		readPosts = data.read;
	}).done(function(){
		comments.each(function(i, item){
			var buttonList = $(item).find('.flat-list').first();
			var id = $(item).find('input[name="thing_id"]').val();

			if(readPosts.indexOf('thing_' + id) > -1){
				$(this).slideUp('fast', function(){
					$(this).addClass('readdit-hidden');
				});
			}else{
				$(buttonList).append('<li><a href="javascript:void(0)" class="readdit-mark-as-read" data-comment-id="' + id + '">mark as read</a></li>');
			}
		});

		$('.readdit-mark-as-read').on('click', function(){
			var id = 'thing_' + $(this).data('comment-id');
			
			$('#' + id).animate({
				opacity: .3,
			}, 'slow', function(){
				$(this).delay(200).slideUp('fast', function(){
					$.post('https://readdit.eastoh.co/api/read', {
						reddit_username: username,
						post_id: id,
						main_post_id: post_id
					}, function(data){
						$('#' + id).addClass('readdit-hidden');
					});
				});
			})
		});
	});

	$('.readdit-show-read').on('click', function(){
		var content = $('#siteTable_t3_' + post_id);
		var isShown = $(this).hasClass('readdit-hide-read');

		if(isShown){
			$(this).text('show already read comments');
			$(this).removeClass('readdit-hide-read');
			$('.readdit-hidden').slideUp('fast');
		}else{
			$(this).text('hide already read comments');
			$(this).addClass('readdit-hide-read');
			$('.readdit-hidden').slideDown('fast');
		}
	});
});